/*
1. Опишіть своїми словами що таке Document Object Model (DOM).
  DOM це побудоване браузером дерево елементів та тексту під час парсингу html. По факту всі теги та текст це об'єкти, з якими можна 
  взаємодіяти за допомогою JS.  

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText??
  Element.innerHTML відобразить в строці текст та всі інші HTML теги, які містяться всередині елементу.
  Element.innerText відобразить текст елементу, а також текст усіх вкладених тегів, не відображаючи самих тегів.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
  До елементів можна звертатись за допомогою старих функцій document.getElementById/ClassName/TagName/ (один елемент) document.getElementsBy (декілька)
  Або кращий спосіб document.querySelector(".ClassName/#id/tagName") чи document.querySelectorAll
*/

"use strict";
// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphCollection = document.querySelectorAll("p"); 
// також можна використати document.getElementsByTagName("p"), але тоді метод forEach не працюватиме і доведеться використовувати for of
paragraphCollection.forEach(paragraph => paragraph.classList.add("paragraph-bg"));
//Інший спосіб paragraphCollection.forEach(paragraph => paragraph.style.backgroundColor = "#ff0000");

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsListElement = document.querySelector("#optionsList");
//Інший варіант document.getElementById("optionsList");
console.log(optionsListElement);
const parentOptionsList = optionsListElement.parentNode;
console.log(parentOptionsList);
const childNodesOptionsList = optionsListElement.children;
for (Element of childNodesOptionsList) {
  console.log(Element);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// В наданому index.html відсутній параграф з класом testParagraph, але є параграф з таким id. Тому спочатку додав параграфу потрібний клас. 
document.querySelector("#testParagraph").classList.add("testParagraph");
const testParagraphElement = document.querySelector(".testParagraph");
testParagraphElement.innerHTML = "This is a paragraph";
console.log(testParagraphElement);

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeaderCollection = document.querySelector(".main-header");
const mainHeaderChildCollection = mainHeaderCollection.children;
for (let Element of mainHeaderChildCollection) {
  console.log(Element);
  Element.classList.add("nav-item");
}

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitleCollection = document.querySelectorAll(".section-title");
// елементи з таким класом відсутні, оскільки отримуємо пусту колекцію. Але якщо такі з'являться, цей клас буде видалено
sectionTitleCollection.forEach(Element => Element.classList.remove("section-title"));




